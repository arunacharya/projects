<?php
header("Access-Control-Allow-Origin: *");
include("connection.php");

$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : 0;
$is_multi = isset($_POST['is_multi']) ? $_POST['is_multi'] : "";
$market_id = isset($_POST['market_id']) ? $_POST['market_id'] : 0; 
$event_cat_id = isset($_POST['event_cat_id']) ? $_POST['event_cat_id'] : 0;
$event_id = isset($_POST['event_id']) ? $_POST['event_id'] : 0;
$stakes = isset($_POST['stakes']) ? $_POST['stakes'] : 0;
$odds = isset($_POST['odds']) ? $_POST['odds'] : 0;
$price = isset($_POST['price']) ? $_POST['price'] : 0;
$event_date = isset($_POST['event_date']) ? $_POST['event_date'] : 0;

if($user_id == 0 || $is_multi == "" || $market_id == 0 || $event_cat_id == 0 || $stakes == 0 || $odds == 0 || $event_id == 0){
    header('Content-Type: application/json');
    echo json_encode(array("message"=>'fail')); 
}
else{
     if($is_multi == 1){
          $now = date("Y-m-d H:i:s");
          $arr = explode(",",$event_date);
          $heighest_date = get_max_date($arr);
          $sql_insert1 = "INSERT INTO `devdb`.`membertips` (`member_id`,`bettype`,`multistake`,`price`,`added`,`expires`) VALUES ('$user_id','multi','$stakes','$price','$now','$heighest_date')";      
          $res = mysqli_query($con,$sql_insert1);
          $insert_id = mysqli_insert_id($con);
          $arr_event_cat = explode(",",$event_cat_id); 
          $arr_event = explode(",",$event_id);
          $arr_market = explode(",",$market_id);  
          $arr_odds = explode(",",$odds);
          for($i=0;$i<count($arr);$i++){
				 $market_ids = $arr_market[$i];
				 $oddss = $arr_odds[$i];
				 $event_cats = $arr_event_cat[$i];
				 $events_ids = $arr_event[$i];
				 $sql_tips_item = "INSERT INTO `devdb`.`tipitems` (`tip_id`,`member_id`,`market_id`,`stake`,`odds`,`line`,".
				 "`cat_id`,`event_id`,`is_void`,`is_marked`,`check_required`,`result`) VALUES ".
				  "('$insert_id','$user_id','$market_ids','1','$oddss','','$event_cats','$events_ids','0','0','0','0')";	
				  mysqli_query($con,$sql_tips_item);
          }
          header('Content-Type: application/json');
    		 echo json_encode(array("message"=>'success')); 
     }
     else{
          $now = date("Y-m-d H:i:s");
          $arr = explode(",",$event_date);
          $heighest_date = get_max_date($arr);
          $sql_insert1 = "INSERT INTO `devdb`.`membertips` (`member_id`,`bettype`,`multistake`,`price`,`added`,`expires`) VALUES ('$user_id','single','0','$price','$now','$heighest_date')";
          $res = mysqli_query($con,$sql_insert1);
          $insert_id = mysqli_insert_id($con);
          $arr_event_cat = explode(",",$event_cat_id); 
          $arr_event = explode(",",$event_id);
          $arr_market = explode(",",$market_id);  
          $arr_odds = explode(",",$odds);
          $arr_stakes = explode(",",$stakes);
          for($i=0;$i<count($arr);$i++){
				 $market_ids = $arr_market[$i];
				 $oddss = $arr_odds[$i];
				 $event_cats = $arr_event_cat[$i];
				 $events_ids = $arr_event[$i];
				 $stakes_vals = $arr_stakes[$i];
				 $sql_tips_item = "INSERT INTO `devdb`.`tipitems` (`tip_id`,`member_id`,`market_id`,`stake`,`odds`,`line`,".
				 "`cat_id`,`event_id`,`is_void`,`is_marked`,`check_required`,`result`) VALUES ".
				  "('$insert_id','$user_id','$market_ids','$stakes_vals','$oddss','','$event_cats','$events_ids','0','0','0','0')";	
				  mysqli_query($con,$sql_tips_item);
          }
          header('Content-Type: application/json');
    		 echo json_encode(array("message"=>'success'));
     }
}



function get_max_date($dates){
$mostRecent= 0;
$pos = 0;
foreach($dates as $key => $date){
  $curDate = strtotime($date);
  if ($curDate > $mostRecent) {
     $mostRecent = $curDate;
     $pos = $key;
  }
}
return $dates[$pos];
}
?>