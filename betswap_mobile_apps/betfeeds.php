<?php

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
include("connection.php");

$soprt_id = isset($_POST['sport_id']) ? $_POST['sport_id'] : 1215;
$league_id = isset($_POST['league_id']) ? $_POST['league_id'] : 4086; 
$page = isset($_POST['page']) ? $_POST['page'] : 1;
$results = array();
$count = 1; 
if($soprt_id == 0 && $league_id == 0){

     $sql = "SELECT * FROM membertips where expires > NOW() order by expires ASC limit 30";	
     $res = mysqli_query($con,$sql);
     $j = 0;
     while($post = mysqli_fetch_assoc($res)){
     	   $id = $post['id'];
     	   $results[$j]['id'] = $id;
     	   $results[$j]['member_id'] = $post['member_id'];
     	   $results[$j]['bettype'] = $post['bettype'];
     	   $results[$j]['multistake'] = $post['multistake'];
     	   $results[$j]['price'] = $post['price'];  
     	   $sql_ti = "select * from tipitems where tip_id = '$id'";
     	   $res_ti = mysqli_query($con,$sql_ti);
     	   $i = 0;
     	   while($p = mysqli_fetch_assoc($res_ti)){
     	   	$results[$j]['tips_item'][$i]['market_id'] = $p['market_id']; 
     	   	$results[$j]['tips_item'][$i]['event_id'] = $p['event_id']; 
     	   	$results[$j]['tips_item'][$i]['cat_id'] = $p['cat_id'];
     	   	$results[$j]['tips_item'][$i]['odds'] = $p['odds']; 
     	   	$results[$j]['tips_item'][$i]['stake'] = $p['stake'];  
     	   	$i++; 
     	   }
     	   $j++; 
     }	
}
elseif($soprt_id != 0 && $league_id == 0){
	$sql_get_league = "SELECT id FROM categories where parent_id = '$soprt_id'"; 
	$res_get_league = mysqli_query($con,$sql_get_league);
   $cat_arr = array();
   $cat_str = "0";
	while($cp = mysqli_fetch_assoc($res_get_league)){
	    $cat_arr[] = $cp['id'];
	}
	if(count($cat_arr) > 0){
	    $cat_str = implode(",",$cat_arr);
	}
	$sql_get_tid = "SELECT distinct tip_id FROM tipitems where cat_id in ($cat_str)"; 
	$res_get_tid = mysqli_query($con,$sql_get_tid);
   $tid_arr = array();
   $tid_str = "0";
   while($ip = mysqli_fetch_assoc($res_get_tid)){
	    $tid_arr[] = $ip['tip_id'];
	}
	if(count($tid_arr) > 0){
	    $tid_str = implode(",",$tid_arr);
	}
	$sql_count = "SELECT * FROM membertips where id in ($tid_str) and expires > NOW() order by expires ASC";
	$r_count = mysqli_query($con,$sql_count);
	$count = mysqli_num_rows($r_count);
	$page = $page-1;
	$sql = "SELECT * FROM membertips where id in ($tid_str) and expires > NOW() order by expires ASC limit $page , 30";	
     $res = mysqli_query($con,$sql);
     $j = 0;
     while($post = mysqli_fetch_assoc($res)){
     	   $id = $post['id'];
     	   $results[$j]['id'] = $id;
     	   $results[$j]['member_id'] = $post['member_id'];
     	   $results[$j]['bettype'] = $post['bettype'];
     	   $results[$j]['multistake'] = $post['multistake'];
     	   $results[$j]['price'] = $post['price'];  
     	   $sql_ti = "select * from tipitems where tip_id = '$id'";
     	   $res_ti = mysqli_query($con,$sql_ti);
     	   $i = 0;
     	   while($p = mysqli_fetch_assoc($res_ti)){
     	   	$results[$j]['tips_item'][$i]['market_id'] = $p['market_id']; 
     	   	$results[$j]['tips_item'][$i]['event_id'] = $p['event_id']; 
     	   	$results[$j]['tips_item'][$i]['cat_id'] = $p['cat_id'];
     	   	$results[$j]['tips_item'][$i]['odds'] = $p['odds']; 
     	   	$results[$j]['tips_item'][$i]['stake'] = $p['stake'];  
     	   	$i++; 
     	   }
     	   $j++; 
     }
}
elseif($soprt_id != 0 && $league_id != 0){
	$sql_get_tid = "SELECT distinct tip_id FROM tipitems where cat_id = $league_id"; 
	$res_get_tid = mysqli_query($con,$sql_get_tid);
   $tid_arr = array();
   $tid_str = "0";
   while($ip = mysqli_fetch_assoc($res_get_tid)){
	    $tid_arr[] = $ip['tip_id'];
	}
	if(count($tid_arr) > 0){
	    $tid_str = implode(",",$tid_arr);
	}
	$sql_count = "SELECT * FROM membertips where id in ($tid_str) and expires > NOW() order by expires ASC";
	$r_count = mysqli_query($con,$sql_count);
	$count = mysqli_num_rows($r_count);
	$page = $page-1;
	$sql = "SELECT * FROM membertips where id in ($tid_str) and expires > NOW() order by expires ASC limit $page , 30";	
     $res = mysqli_query($con,$sql);
     $j = 0;
     while($post = mysqli_fetch_assoc($res)){
     	   $id = $post['id'];
     	   $results[$j]['id'] = $id;
     	   $results[$j]['member_id'] = $post['member_id'];
     	   $results[$j]['bettype'] = $post['bettype'];
     	   $results[$j]['multistake'] = $post['multistake'];
     	   $results[$j]['price'] = $post['price'];  
     	   $sql_ti = "select * from tipitems where tip_id = '$id'";
     	   $res_ti = mysqli_query($con,$sql_ti);
     	   $i = 0;
     	   while($p = mysqli_fetch_assoc($res_ti)){
     	   	$results[$j]['tips_item'][$i]['market_id'] = $p['market_id']; 
     	   	$results[$j]['tips_item'][$i]['event_id'] = $p['event_id']; 
     	   	$results[$j]['tips_item'][$i]['cat_id'] = $p['cat_id'];
     	   	$results[$j]['tips_item'][$i]['odds'] = $p['odds']; 
     	   	$results[$j]['tips_item'][$i]['stake'] = $p['stake'];    
     	   	$i++; 
     	   }
     	   $j++; 
     } 
}
if(count($results) <= 0){
	echo json_encode(array('status'=>0,'total_pages'=>0,'data'=>array()));
}
else{
   echo json_encode(array('status'=>1,'total_pages'=>$count,'data'=>$results));
}
?>