<?php
header("Access-Control-Allow-Origin: *");
include("connection.php");

$sql = "SELECT * FROM categories WHERE parent_id = 0 ORDER BY name";
$res = mysqli_query($con,$sql);
$result = array();
$i = 0;
while($post=mysqli_fetch_assoc($res)){

	$result[$i]['id'] = $post['id'];
   $result[$i]['name'] = $post['name'];   
   $i++;
}

header('Content-Type: application/json');
if(count($result)<0)
   echo json_encode(array("status"=>0,"data"=>array()));
else 
   echo  json_encode(array("status"=>1,"data"=>$result));   
?>