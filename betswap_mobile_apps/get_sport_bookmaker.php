<?php
header("Access-Control-Allow-Origin: *");
include("connection.php");
$main = array();
$sql_sport = "SELECT * FROM sports";
$res_sport = mysqli_query($con,$sql_sport);
$i = 0; 
while($results = mysqli_fetch_assoc($res_sport)){
    $main['sport'][$i]['id'] = $results['id'];
    $main['sport'][$i]['name'] = $results['sport'];
    $i++; 
}
$sql_bookmaker = "SELECT * FROM bookmakers";
$res_bookmaker = mysqli_query($con,$sql_bookmaker);
$i = 0;
while($res = mysqli_fetch_assoc($res_bookmaker)){
    $main['bookmaker'][$i]['id'] = $res['id'];
    $main['bookmaker'][$i]['name'] = $res['name'];
    $main['bookmaker'][$i]['url'] = $res['url'];
    $main['bookmaker'][$i]['logo_url'] = $res['logo_url'];
    $i++; 

}
$main['avg_week_spend'][0] = "<$100";
$main['avg_week_spend'][1] = "$100-$250";
$main['avg_week_spend'][2] = "$250-$500";
$main['avg_week_spend'][3] = "$500-$1000";
$main['avg_week_spend'][4] = "$1000+";
header('Content-Type: application/json');
echo json_encode(array("status"=>1,"data"=>$main));
?>