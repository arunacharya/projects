<?php 
/* Template Name: training */
//code
 get_header();
?>
 <section id="service_parallax" class="section-white page-title-wrapper" data-stellar-background-ratio="1" data-stellar-offset-parent="true">
			<div class="container">
				<div class="relative">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title text-center">
							<h4>Training</h4>
							<hr>
							<p>Awesome Training</p>
							<ol class="breadcrumb">
							  <li><a href="#">Home</a></li>
							  <li class="active">Training</li>
							</ol>
							</div><!-- end title -->
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- end relative -->
			</div><!-- end container -->
		</section><!-- end section-white -->

		<section class="section-white feature">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
						 	<?php
	$my_query = new WP_Query('category_name=about_us&post_per_page=1&order=ASC'); 
    while ($my_query->have_posts()) :
    $my_query->the_post();
    $do_not_duplicate = $post->ID;
	$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
    ?>
			
						<div class="widget-title">
								<h4><?php echo get_the_title(); ?></h4>
								<hr>
							</div><!-- end widget-title -->
							<img src="<?php echo $image[0]; ?>" alt="" class="img-responsive">
							<p><?php echo get_the_content(); ?></p>
							<?php 
    //$p++;
    endwhile;
    ?> 
						
						</div><!-- end widget -->
					</div><!-- end col -->

					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
							<div class="widget-title">
								<h4>Why Us</h4>
								<hr>
							</div><!-- end widget-title -->
                            <div class="accordion-toggle-2">
                                <div class="panel-group" id="accordion">
								<?php
		             $args= array('post_type'=> 'why_us', 'posts_per_page'=>3,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#<?php echo get_post_meta(get_the_ID(),'id',true);?>">
                                                    <h3><i class="indicator fa <?php echo get_post_meta(get_the_ID(),'icon',true);?>"></i><?php echo get_the_title(); ?></h3>
                                                </a>
                                            </div>
                                        </div>
                                        <div id="<?php echo get_post_meta(get_the_ID(),'id',true);?>" class="panel-collapse collapse <?php echo get_the_excerpt(); ?>">
                                            <div class="panel-body">
                                                <p><?php echo get_the_content(); ?></p>
                                            </div>
                                        </div>
                                    </div>
									 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
                                    
                                </div>
                            </div><!-- accordion -->
						</div><!-- end widget -->
					</div><!-- end col -->

					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
							<div class="widget-title">
								<h4>Skills</h4>
								<hr>
							</div><!-- end widget-title -->
		                    <div class="skills">
							<?php
		             $args= array('post_type'=> 'skill_training', 'posts_per_page'=>5,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>
		                        <p><?php echo get_the_title(); ?></p>
		                        <div class="progress active">
		                            <div class="progress-bar" role="progressbar" data-transitiongoal="<?php echo get_the_content(); ?>"><span><?php echo get_the_content(); ?>%</span></div>
		                        </div>
								 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
		                    
		                    </div><!-- end skills -->
						</div><!-- end widget -->
					</div><!-- end col -->
				</div><!-- end row -->

				<div class="row section-container">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
							<div class="widget-title">
								<h4>Our Clients</h4>
								<hr>
							</div><!-- end widget-title -->

		                    <div id="clients" class="clearfix text-center">
		                    	<?php 
   $args= array('post_type'=> 'clients', 'posts_per_page'=>12,'order'=>'ASC');
   query_posts($args);		
$p=1;
if ( have_posts() ) : while ( have_posts() ) : the_post(); 
$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 

	?>
	<div class="client-image">
		                    		<a href="#"><img src="<?php echo $image[0]; ?>" alt="" class="img-responsive"></a>
		                    	</div>
	<?php 
		$p++;
		endwhile;
		endif;
	?>
		                    </div><!-- end clients -->
						</div><!-- end widget -->
					</div><!-- end col -->
				</div><!-- end row -->
			</div><!-- end container -->
		</section><!-- end section-white -->

	    <section id="shortcodes" class="section-white search-wrapper feature" data-stellar-background-ratio="1" data-stellar-offset-parent="true">
			<div class="overlay"></div>
			<div class="container">
				<div class="relative">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title text-center">
							<h4>Search for Courses</h4>
							<hr>
							<p>Fill The Below Forms And Start Searching</p>
							</div><!-- end title -->
						</div><!-- end col -->
					</div><!-- end row -->

					<div class="section-container">
						<form class="row search_form">
							<div class="col-md-3 col-sm-6">
							    <input type="text" class="form-control" placeholder="Search Words">
							</div>
							<div class="col-md-3 col-sm-6">
								<select class="selectpicker form-control" data-style="btn-inverse">
						  			<option>Category</option>
									<option>Designing</option>
							        <option>Development</option>
								</select>
							</div>
							<div class="col-md-3 col-sm-6">
								<select class="selectpicker form-control" data-style="btn-inverse">
						  			<option>Type</option>
									<option>Android</option>
							        <option>PHP</option>
								</select>
							</div>
							<div class="col-md-3 col-sm-6">
								<button type="submit" class="btn btn-default btn-block">Start Search</button>
							</div>
						</form>
					</div><!-- end row -->
				</div><!-- end relative -->
			</div><!-- end container -->
		</section><!-- end section-white -->


		<section class="section-grey feature">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title text-center">
						<h4>Fun Facts</h4>
						<hr>
						<p>Things You Must Know About Us</p>
						</div><!-- end title -->
					</div><!-- end col -->
				</div><!-- end row -->

				<div class="row section-container">
					
<?php
		             $args= array('post_type'=> 'fun_fact', 'posts_per_page'=>4,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>
                       <div class="col-md-3 col-sm-6">
						<div class="fun-fact-box text-center wow fadeIn" data-wow-duration="1s" data-wow-delay="<?php echo get_post_meta(get_the_ID(),'data_wow_delay',true);?>s">
							<i class="fa <?php echo get_post_meta(get_the_ID(),'icon',true);?> fa-3x"></i>
							<h4><span class="stat-count"><?php echo get_post_meta(get_the_ID(),'count',true);?></span> +</h4>
							<h5><?php echo get_the_title(); ?></h5>
						</div><!-- end box -->
					</div><!-- end col -->         
									 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
				
				</div><!-- end row -->
			</div>
		</section>


		<section class="section-white service-relative-wrapper background">
			<div class="container">
				<div class="service-relative">
					<div class="row">
					<?php
		             $args= array('post_type'=> 'services_redbox', 'posts_per_page'=>3,'order'=>'DESC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 $image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
					 ?>
					 <div class="col-md-4 col-sm-6 col-xs-12">
							<div class="service-style-1">
								<div class="icon-normal wow fadeIn" data-wow-duration="1s" data-wow-delay="<?php echo get_post_meta(get_the_ID(),'data_wow_delay',true);?>s">
									<i class="icon <?php echo get_post_meta(get_the_ID(),'icon',true);?>"></i>
								</div><!-- end icon-wrap -->
								<div class="title-wrap">
									<h5 class="service-title"><?php echo get_the_title(); ?></h5>
									<hr>
									<p><?php echo get_the_content(); ?></p>
								</div><!-- end title-wrap -->
							</div><!-- end title -->
						</div><!-- end col -->      
									 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
					</div><!-- end row -->
				</div><!-- end background service-absolute -->
			</div><!-- end container -->
		</section><!-- end section-white -->
		 
		<div class="wrapper w9">
		     <div class="container">
			    <div class="row">
				 	<?php
	$my_query = new WP_Query('category_name=request_quote&post_per_page=1&order=ASC'); 
    while ($my_query->have_posts()) :
    $my_query->the_post();
    $do_not_duplicate = $post->ID;
	$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
    ?>
	<div class="col-md-8 quote">
					    <?php echo get_the_content(); ?>
					</div>	
					 <div class="col-md-4 request">	
					   <a href="<?php echo home_url('/?page_id=14');?>" class="button5 b5 "><?php echo get_the_title(); ?></a>
					</div>
							<?php 
    //$p++;
    endwhile;
    ?> 
				    
	           </div>
             </div>			   
		 </div>
<?php get_footer(); ?>