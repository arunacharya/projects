<?php 
/* Template Name: Services */
//code
 get_header();
?>
 <!-- slide Section start-->
 <section id="about_parallax" class="section-white page-title-wrapper" data-stellar-background-ratio="1" data-stellar-offset-parent="true">
			<div class="container">
				<div class="relative">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title text-center">
							<h4>Services</h4>
							<hr>
							<p>All You Want To Know Our Services</p>
							<ol class="breadcrumb">
							  <li><a href="#">Home</a></li>
							  <li class="active">Services</li>
							</ol>
							</div><!-- end title -->
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- end relative -->
			</div><!-- end container -->
		</section><!-- end section-white -->


		<section class="section-grey feature">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title text-center">
						<h4>Our Features</h4>
						<hr>
						<p>Lorem ipsum dolor sit amet, consectetur adipiing elit</p>
						</div><!-- end title -->
					</div><!-- end col -->
				</div><!-- end row -->

				<div class="row section-container">
				<?php
		             $args= array('post_type'=> 'features', 'posts_per_page'=>6,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="service-style-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="<?php echo get_post_meta(get_the_ID(),'data_wow_delay',true);?>s">
							<div class="icon-wrap">
								<i class="fa <?php echo get_post_meta(get_the_ID(),'icon',true);?>"></i>
							</div><!-- end icon-wrap -->
							<div class="title-wrap">
								<h5 class="service-title"><?php echo get_the_title(); ?></h5>
								<hr>
								<p><?php echo get_the_content(); ?><a href="#">Read More</a></p>
							</div><!-- end title-wrap -->
						</div><!-- end title -->
					</div><!-- end col -->
 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
				
				</div><!-- end row -->		
			</div><!-- end container -->
		</section><!-- end section-white -->


	    <section id="parallax4" class="section-white fullscreen feature" data-stellar-background-ratio="0.6" data-stellar-offset-parent="true">
			<div class="container relative">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title text-center">
						<h4>Our Services</h4>
						<hr>
						<p>Best And Unique Services For You</p>
						</div><!-- end title -->
					</div><!-- end col -->
				</div><!-- end row -->

				<div id="owl-services" class="owl-custom section-container">
				<?php
		             $args= array('post_type'=> 'services', 'posts_per_page'=>8,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>
					<div class="service-item text-center">
						<div class="rounded-icon">
							<i class="icon <?php echo get_the_excerpt(); ?>"></i>
						</div><!-- end rounded-icon -->
						<div class="service-desc">
							<h4><?php echo get_the_title(); ?></h4>
							<hr>
							<p><?php echo get_the_content(); ?> <a href="#">Read More</a></p>
						</div><!-- end service-desc -->
					</div><!-- end item -->
 <?php 
				     //$p++;
				     endwhile;
		             endif;?>	
				</div><!-- end services -->
			</div>
		</section>
	
		
		 <div class="wrapper w9">
		     <div class="container">
			    <div class="row">
				<?php
	$my_query = new WP_Query('category_name=request_quote&post_per_page=1&order=ASC'); 
    while ($my_query->have_posts()) :
    $my_query->the_post();
    $do_not_duplicate = $post->ID;
	$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
    ?>
				    <div class="col-md-8 quote">
					    <?php echo get_the_content(); ?>
					</div>	
					 <div class="col-md-4 request">	
					   <a href="<?php echo home_url('/?page_id=14');?>" class="button5 b5 "><?php echo get_the_title(); ?></a>
					</div>
					<?php 
    //$p++;
    endwhile;
    ?> 
	           </div>
             </div>			   
		 </div>
		
<!-- /Services Section -->
<?php get_footer(); ?>