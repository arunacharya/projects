<?php 
/* Template Name: Portfolio */
 get_header();
?>
 <section id="contact_parallax" class="section-white page-title-wrapper" data-stellar-background-ratio="1" data-stellar-offset-parent="true">
			<div class="container">
				<div class="relative">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title text-center">
							<h4>Portfolio</h4>
							<hr>
							<p>We Wanna Hear From You </p>
							<ol class="breadcrumb">
							  <li><a href="#">Home</a></li>
							  <li class="active">Portfolio</li>
							</ol>
							</div><!-- end title -->
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- end relative -->
			</div><!-- end container -->
		</section><!-- end section-white -->

	<div class="filter_header_wrap wrapper w3 ">
	<div class="container c8">
		<div class="row ">
				<div class="filter_cat section">
				      <navvv class="cl-effect-14 dark" id="cl-effect-14">
				          <a href="#cl-effect-14"><span class="change">  OUR</span> PORTFOLIO</a>
					
				      </navvv>
					 
					  <img src="<?php echo get_template_directory_uri(); ?>/image/shape3.png">
					<div class="work-filter wow fadeInRight animated" data-wow-duration="500ms">
						<ul class="text-center text">
							<li><a href="javascript:;" data-filter="all" class="filter button2 b2 active">All</a></li>
							<li><a href="javascript:;" data-filter=".animation" class="filter button2 b2">ANIMATION</a></li>
							<li><a href="javascript:;" data-filter=".website" class="filter button2 b2">WEBSITE</a></li>
							
						</ul>
					</div>
				</div>			
		</div>
	</div>
	
<div class="project-wrapper" id="MixItUp838317">

			<div class="container">
             <div class="row r2">
			 <?php 
		             $args= array('post_type'=> 'portfolio', 'posts_per_page'=>9,'order'=>'ASC');
		             query_posts($args);		
					 $p=1;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 $image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
					?>
				 <figure class=" col-md-4  col-lg-4  col-sm-4 mix work-item <?php echo get_the_excerpt(); ?> wv_20" style="display: inline-block;">
					<div class="grid">
					 <div class="col-md-4 col-lg-4  col-sm-4 option2">
					<figure class="effect-winston">
						<img src="<?php echo $image[0]; ?>" >
						<figcaption>
							 <h2><?php echo get_the_title(); ?><span><?php echo get_the_content(); ?></span></h2>
											<p>
									         <!--<a href="#" class="button3 b3"><i class="fa fa-fw fa-star-o"></i></a>	
						                     <a href="#" class="button3 b3"><i class="fa fa-fw fa-comments-o"></i></a>-->
						                     <a href="<?php echo get_permalink(); ?>"  class="button3 b3"><i class="fa fa-external-link"></i></a>
                                           </p>
						</figcaption>			
					</figure>
					</div>
					</div>
					<figcaption class="overlay">		
					</figcaption>
				</figure>	
	<?php 
		$p++;
		endwhile;
		endif;
	?>
	
				
			</div>
          </div>			
	 </div>
</div>
<?php get_footer(); ?>