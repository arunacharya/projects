<?php 
/* Template Name: technology_framework */
//code
 get_header();
?>
 <section id="faqs_parallax" class="section-white page-title-wrapper" data-stellar-background-ratio="1" data-stellar-offset-parent="true">
			<div class="container">
				<div class="relative">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title text-center">
							<h4>Technologies/Framework</h4>
							<hr>
							<p>lots of freamework</p>
							<ol class="breadcrumb">
							  <li><a href="#">Home</a></li>
							  <li class="active">Technologies/Framework</li>
							</ol>
							</div><!-- end title -->
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- end relative -->
			</div><!-- end container -->
		</section><!-- end section-white -->

		
		<section class="section-white">
			<div class="container">
	<div class="section_holder68">
		<div class="one_full">
          <ul class="tabs">
            <li><a href="#demo-tab-1" target="_self">Animation</a></li>
            <li><a href="#demo-tab-2" target="_self">IOS</a></li>
            <li><a href="#demo-tab-3" target="_self">Magento</a></li>
			<li><a href="#demo-tab-4" target="_self">Drupal</a></li>
            <li><a href="#demo-tab-5" target="_self">Jhoomla</a></li>
            <li><a href="#demo-tab-6" target="_self">Wordpress</a></li>
            <li><a href="#demo-tab-7" target="_self">Iphone</a></li>
						 <li><a href="#demo-tab-8" target="_self">Html5</a></li>
          </ul>
          <div class="tabs-content">
		  <?php
		             $args= array('post_type'=> 'technology','posts_per_page'=>8,'order'=>'ASC');
					 
		             query_posts($args);		
					
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>
		<div id="<?php echo get_the_excerpt(); ?>" class="tabs-panel">
              <?php echo get_the_content(); ?>
              
            </div>
            <!-- end tab 1 -->
			<?php
				     endwhile;
		             endif;?>
            
          </div>
        </div>
		</div>				
				</div><!-- end container -->
		</section><!-- end section-white -->
		
	
		<div class="wrapper w9 wra">
		     <div class="container">
			    <div class="row">
					<?php
	$my_query = new WP_Query('category_name=request_quote&post_per_page=1&order=ASC'); 
    while ($my_query->have_posts()) :
    $my_query->the_post();
    $do_not_duplicate = $post->ID;
	$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
    ?>
				    <div class="col-md-8 quote">
					    <?php echo get_the_excerpt(); ?>
					</div>	
					 <div class="col-md-4 request">	
					  <a href="<?php echo home_url('/?page_id=14');?>" class="button6 b5 "><?php echo get_the_title(); ?></a>
					</div>
					<?php 
    //$p++;
    endwhile;
    ?> 
	           </div>
             </div>			   
		 </div>
<?php get_footer(); ?>