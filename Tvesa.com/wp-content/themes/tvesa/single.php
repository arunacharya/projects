<?php
/**
 * The template for displaying all single posts.
 *
 * @package tvesa
 */
$id="";
get_header(); ?>

	<!--<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">-->

		<?php while ( have_posts() ) : the_post(); ?>

			<?php //get_template_part( 'template-parts/content', 'single' ); ?>
             <?php $id=get_post_meta(get_the_ID(),'behance_id',true);?>
			 <!--<input type="hidden" id="id_hidden" value="<?php //echo $id;?>">-->
<!-- slide Section start-->
<section id="contact_parallax" class="section-white page-title-wrapper" data-stellar-background-ratio="1" data-stellar-offset-parent="true">
			<div class="container">
				<div class="relative">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title text-center">
							<h4 class="page_name1"><?php if(empty($id)){ echo get_the_title();}?></h4>
							<hr>
							<p>We Wanna Hear From You </p>
							</div><!-- end title -->
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- end relative -->
			</div><!-- end container -->
		</section><!-- end section-white -->

	<!-- Services Section -->
<div class="image_wrap">
	<div class="container">
		<div class="row">
				<div id="bop-all-wrapper">
	
	<div class="bop-project-area" style="background-color: #ffffff !important;">
		<div class="bop-project-wrapper">
			<div class="bop-project-left">
				<div class="bop-left-project-header">
										<span class="bop-header-title page_name1"><?php if(empty($id)){ echo get_the_title();}?></span>					
															<h5 class="fields"></h5>
										
				</div>
				
				<div class="bop-primary-project-content">
					<div class="spacer" style="height: 50px">
					</div>
					<div class="bop-text-center" id="add_img">
					<?php if(empty($id)){ $image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );?> <img src="<?php echo $image[0]; ?>" alt=""> <?php }?>
									
						</div>
						<div class="spacer" style="height: 60px">
																																										<div class="divider" style="display:none">
							</div>
						</div>						
											<!--<div class="bop-text-center">
													<div>&nbsp;</div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="color:#f6f6f6;" target="_blank">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a></div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="color:#f6f6f6;" target="_blank">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a></div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="color:#f6f6f6;" target="_blank">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a></div>						
									
						</div>
						<div class="spacer" style="height: 60px">
																																										<div class="divider" style="display:none">
							</div>
						</div>	-->					
									</div>
			</div>
			<div id="bop-project-right">
								
				
				<div id="bop-project-about-right" class="bop-project-right">
											<h5>About Project :</h5>
										<div class="bop-short-desc">
										<p class="desc"><?php if(empty($id)){ echo get_the_content(); }?></p>
							</div>
							
																					
					<h6 class="pub_date"></h6>
										
				</div>
				
								
								<div id="bop-project-short-link" class="bop-project-right">
					<h6><i class="fa fa-link fa-lg"> </i>&nbsp; <a target="_blank" href="#" class="shotrlink"></a></h6>
				</div>
						
				
								<div id="bop-project-short-link" class="bop-project-right tags_prj">
					<h5>Project Tags:</h5>					

										
				</div>	
						
				
				
				
				<div id="bop-project-info-right" class="bop-project-right">
						
					<h5 class="tool_used">
						<i class="fa fa-cogs fa-lg"> </i>&nbsp; 
					</h5>
										
																
					<h5><i class="fa fa-warning fa-lg"> </i>&nbsp; 
					<a href="#" target="_blank">Report</a></h5>
				</div>			
			</div>
		</div>
	</div>
</div>
	
		</div>
	</div>

</div>
			 
			 
			<?php //the_post_navigation(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				//if ( comments_open() || get_comments_number() ) :
				//	comments_template();
				//endif;
			?>

		<?php endwhile; // End of the loop. ?>

		<!--</main>
	</div>--><!-- #primary -->


<?php get_footer(); ?>
	<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
$.ajax({
      type: "GET",
      dataType: "json",
      url: "https://www.behance.net/v2/projects/<?php echo $id;?>?callback=?&api_key=kdiIQSTLe8X6fKPq59BVdEhnG5YHVtb1", //Relative or absolute path 
	  crossDomain: true,
      success: function(data) {
        //alert(data['project']['id']);
		$('.page_name').html(data['project']['name']);
		$('.page_name1').html(data['project']['name']);
		$('.fields').html(data['project']['fields']);
		$('.desc').html(data['project']['description']);
		$('.pub_date').html(data['project']['published_on']);
		$('.shotrlink').html(data['project']['short_url']);
		$('.shotrlink').attr('href',data['project']['short_url']);
		for(i=0;i<data['project']['modules'].length-1;i++){
		    var a1 = data['project']['modules'][i];
		    var src = a1['src'];
			var html = '<img src="'+src+'" width="100%">';
            $('#add_img').append(html);		
		}
		for(j=0;j<data['project']['tags'].length;j++){
		    var data_tag = data['project']['tags'][j];
			html = '';
			var html = '<a href="#" class="bop-tags">'+data_tag+'</a>';
			$('.tags_prj').append(html);
		}
		var count = 0;
		for(k=0;k<data['project']['tools'].length;k++){
		     count++;
		}
		$('.tool_used').append("Tools Used ("+count+")");
      }
    });
});
</script>