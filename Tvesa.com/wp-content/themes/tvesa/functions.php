<?php
/**
 * tvesa functions and definitions
 *
 * @package tvesa
 */


if ( ! function_exists( 'tvesa_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function tvesa_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on tvesa, use a find and replace
	 * to change 'tvesa' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'tvesa', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'tvesa' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'tvesa_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // tvesa_setup
add_action( 'after_setup_theme', 'tvesa_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tvesa_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tvesa_content_width', 640 );
}
add_action( 'after_setup_theme', 'tvesa_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function tvesa_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'tvesa' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'tvesa_widgets_init' );

//custom sidebar Head Office
function textdomain_register_sidebars() {
    register_sidebar(
        array(
            'id' => 'about',
            'name' => 'ABOUT',
            'description' => '',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );
       register_sidebar(
        array(
            'id' => 'services',
            'name' => 'SERVICES',
            'description' => '',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );
	register_sidebar(
        array(
            'id' => 'technologies',
            'name' => 'TECHNOLOGIES',
            'description' => '',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );
	register_sidebar(
        array(
            'id' => 'training',
            'name' => 'TRAINING',
            'description' => '',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );
		register_sidebar(
        array(
            'id' => 'contact_widget1',
            'name' => 'contact widget1',
            'description' => '',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );
	register_sidebar(
        array(
            'id' => 'contact_widget2',
            'name' => 'contact widget2',
            'description' => '',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );
}
add_action( 'widgets_init', 'textdomain_register_sidebars' );

// testimonial Post
add_action('init','create_testimonial_content_type');
function create_testimonial_content_type()
{
  register_post_type('testimonial',array('labels'=>array('name'=> 'Testimonial','singular_name'=>'Testimonial'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

// Clients Post
add_action('init','create_clients_content_type');
function create_clients_content_type()
{
  register_post_type('clients',array('labels'=>array('name'=> 'Clients','singular_name'=>'Clients'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

// technology Post
add_action('init','create_technology_content_type');
function create_technology_content_type()
{
  register_post_type('technology',array('labels'=>array('name'=> 'Technology','singular_name'=>'Technology'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

// Portfolio Post
add_action('init','create_portfolio_content_type');
function create_portfolio_content_type()
{
  register_post_type('portfolio',array('labels'=>array('name'=> 'Portfolio','singular_name'=>'Portfolio'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));

}
add_theme_support( 'post-thumbnails' );

// features Post
add_action('init','create_features_content_type');
function create_features_content_type()
{
  register_post_type('features',array('labels'=>array('name'=> 'Features','singular_name'=>'Features'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );
// Services Post
add_action('init','create_services_content_type');
function create_services_content_type()
{
  register_post_type('services',array('labels'=>array('name'=> 'Services','singular_name'=>'Services'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

// why_us Post
add_action('init','create_why_us_content_type');
function create_why_us_content_type()
{
  register_post_type('why_us',array('labels'=>array('name'=> 'Why_us','singular_name'=>'Why_us'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

// fun_fact Post
add_action('init','create_fun_fact_content_type');
function create_fun_fact_content_type()
{
  register_post_type('fun_fact',array('labels'=>array('name'=> 'Fun_fact','singular_name'=>'Fun_fact'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

// skill Post
add_action('init','create_skill_content_type');
function create_skill_content_type()
{
  register_post_type('skill',array('labels'=>array('name'=> 'Skill','singular_name'=>'Skill'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

// skill_training Post
add_action('init','create_skill_training_content_type');
function create_skill_training_content_type()
{
  register_post_type('skill_training',array('labels'=>array('name'=> 'Skill_training','singular_name'=>'Skill_training'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

// latest_news Post
add_action('init','create_latest_news_content_type');
function create_latest_news_content_type()
{
  register_post_type('latest_news',array('labels'=>array('name'=> 'Latest_news','singular_name'=>'Latest_news'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

// services_redbox
add_action('init','create_services_redbox_content_type');
function create_services_redbox_content_type()
{
  register_post_type('services_redbox',array('labels'=>array('name'=> 'Services_redbox','singular_name'=>'Services_redbox'),'public'=>true,'has_archive'=>true,'taxonomies'=>array('category'=>'post_tag'),'supports'=>array('title','editor','author','thumbnail','excerpt','comments','custom-fields')));
}
add_theme_support( 'post-thumbnails' );

/**
 * Enqueue scripts and styles.
 */
function tvesa_scripts() {
	wp_enqueue_style( 'tvesa-style', get_stylesheet_uri() );

	wp_enqueue_script( 'tvesa-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'tvesa-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tvesa_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
