<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package tvesa
 */

get_header(); ?>

 
		<section class="slider-section">
			<div class="tp-banner-container">
				<div class="tp-banner">
					<ul>
						<li data-transition="slidevertical" data-slotamount="1" data-masterspeed="500" data-thumb="<?php echo get_template_directory_uri(); ?>/upload/slider_022.jpg"  data-saveperformance="off"  data-title="Slide">
							<img src="<?php echo get_template_directory_uri(); ?>/upload/slider_022.jpg"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
	                        <div class="tp-caption slider_01 text-left skewfromright randomrotateout tp-resizeme"
	                            data-x="10"
	                            data-y="240" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Tvesa, Development <hr>
	                        </div>
	                        <div class="tp-caption slider_02 text-center skewfromright randomrotateout tp-resizeme"
	                            data-x="10"
	                            data-y="330" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Mobile Development
	                        </div>
	                        <div class="tp-caption slider_03 text-left randomrotateout tp-resizeme"
	                            data-x="10"
	                            data-y="395" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Our TVESA mobile technology lab continuously explores rapidly evolving mobility<br> technologies, with a vision to help our customers leverage them for business<br> advantage.
	                        </div>
                            <div class="tp-caption lft customout rs-parallaxlevel-0"
                                data-x="660"
                                data-y="231" 
                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="1000"
                                data-start="1400"
                                data-easing="Power3.easeInOut"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                style="z-index: 4;"><img src="<?php echo get_template_directory_uri(); ?>/images/dummy.png" alt="" data-lazyload="<?php echo get_template_directory_uri(); ?>/image/second_mobile.png">
                            </div>
                            <div class="tp-caption lft customout rs-parallaxlevel-0"
                                data-x="590"
                                data-y="370" 
                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="1000"
                                data-start="1600"
                                data-easing="Power3.easeInOut"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                style="z-index: 4;"><img src="<?php echo get_template_directory_uri(); ?>/images/dummy.png" alt="" data-lazyload="<?php echo get_template_directory_uri(); ?>/image/first_mobile.png">
                            </div>
						</li>
						<li data-transition="slidevertical" data-slotamount="1" data-masterspeed="500" data-thumb="<?php echo get_template_directory_uri(); ?>/upload/slider_03.jpg"  data-saveperformance="off"  data-title="Slide">
							<img src="<?php echo get_template_directory_uri(); ?>/upload/slider_03.jpg"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
	                        <div class="tp-caption slider_01 text-center skewfromright randomrotateout tp-resizeme"
	                            data-x="center"
	                            data-y="40" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Tvesa,Animation <hr>
	                        </div>
	                        <div class="tp-caption slider_02 text-center skewfromright randomrotateout tp-resizeme"
	                            data-x="center"
	                            data-y="130" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Animation System
	                        </div>
	                        <div class="tp-caption slider_03 text-center randomrotateout tp-resizeme"
	                            data-x="center"
	                            data-y="195" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Desire a marvelous Animation along with effective and user friendly UI. Leave all to TVESA For Development.
	                        </div>
							<div class="tp-caption slider_03 text-center randomrotateout tp-resizeme"
	                            data-x="center"
	                            data-y="295" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"> <iframe src="https://player.vimeo.com/video/131534760?title=0&amp;byline=0&amp;portrait=0&amp;player_id=iframe43176&amp;api=1" width="500" height="281" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" class="HasListener" id="iframe43176" style="display: block; width: 500px; height: 281px; transform-style: preserve-3d; z-index: 0; visibility: hidden; opacity: 0; transform: matrix(1, 0, 0, 1, 0, 0);"></iframe>
	                        </div>
						</li>
						<li data-transition="slidevertical" data-slotamount="1" data-masterspeed="500" data-thumb="<?php echo get_template_directory_uri(); ?>/upload/slider_011.jpg"  data-saveperformance="off"  data-title="Slide">
							<img src="<?php echo get_template_directory_uri(); ?>/upload/slider_011.jpg"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
	                        <div class="tp-caption text-center skewfromleft randomrotateout tp-resizeme"
	                            data-x="center"
	                            data-y="150" 
	                            data-speed="1000"
	                            data-start="800"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><img src="<?php echo get_template_directory_uri(); ?>/image/tvesa1.png">
	                        </div>
	                        <div class="tp-caption slider_01 text-center skewfromright randomrotateout tp-resizeme"
	                            data-x="center"
	                            data-y="240" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">Tvesa,Training Future <hr>
	                        </div>
	                        <div class="tp-caption slider_02 text-center skewfromright randomrotateout tp-resizeme"
	                            data-x="center"
	                            data-y="330" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">We Will Help You To Learn
	                        </div>
	                        <div class="tp-caption slider_03 text-center randomrotateout tp-resizeme"
	                            data-x="center"
	                            data-y="395" 
	                            data-speed="1000"
	                            data-start="1400"
	                            data-easing="Power3.easeInOut"
	                            data-splitin="none"
	                            data-splitout="none"
	                            data-elementdelay="0.1"
	                            data-endelementdelay="0.1"
	                            data-endspeed="1000"
	                            style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">TVESA has a professional network to cater to education and career needs of aspiring and experienced<br> IT Professionals. We take pride in having assisted thousands of candidates delivering their remarkable contribution to<br> various IT organizations.
	                        </div>
						</li>
					</ul>
				</div>
			</div>
		</section>

		
<section class="section-white makepadding ">
			<div class="container">
				<div class="relative">
				
					<div class="background service-absolute" >
						<div class="row">
						<?php
		             $args= array('post_type'=> 'services_redbox', 'posts_per_page'=>3,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 $image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
					 ?>
					 <div class="col-md-4 col-sm-6 col-xs-12">
								<div class="service-style-1">
									<div class="icon-normal wow fadeIn" data-wow-duration="1s" data-wow-delay="<?php echo get_post_meta(get_the_ID(),'data_wow_delay',true);?>s" style="<?php echo get_the_excerpt();?>">
										<i class="<?php echo get_post_meta(get_the_ID(),'icon',true);?>"></i>
									</div><!-- end icon-wrap -->
									<div class="title-wrap">
										<h5 class="service-title"><?php echo get_the_title(); ?></h5>
										<hr>
										<p><?php echo get_the_content(); ?></p>
									</div><!-- end title-wrap -->
								</div><!-- end title -->
							</div><!-- end col -->      
									 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
							
						</div><!-- end row -->
						<!--<img src="../../../image/shadow.png">-->
					</div><!-- end background service-absolute -->
					<!--<div class="box effect6">
					</div>-->
				</div><!-- end relative -->
			</div><!-- end container -->
</section>
			
	
	<div class="filter_header_wrap wrapper w3 ">
	<div class="container c8">
		<div class="row ">
				<div class="filter_cat section">
				      <navvv class="cl-effect-14 dark" id="cl-effect-14">
				          <a href="#cl-effect-14"><span class="change">  OUR</span> PORTFOLIO</a>
					
				      </navvv>
					 
					  <img src="<?php echo get_template_directory_uri(); ?>/image/shape3.png">
					<div class="work-filter wow fadeInRight animated" data-wow-duration="500ms">
						<ul class="text-center text">
							<li><a href="javascript:;" data-filter="all" class="filter button2 b2 active">All</a></li>
							<li><a href="javascript:;" data-filter=".animation" class="filter button2 b2">ANIMATION</a></li>
							<li><a href="javascript:;" data-filter=".website" class="filter button2 b2">WEBSITE</a></li>
							
						</ul>
					</div>
				</div>			
		</div>
	</div>
	
<div class="project-wrapper" id="MixItUp838317">

			<div class="container">
             <div class="row r2">
			 <?php 
		             $args= array('post_type'=> 'portfolio', 'posts_per_page'=>9,'order'=>'ASC');
		             query_posts($args);		
					 $p=1;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 $image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
					?>
				 <figure class=" col-md-4  col-lg-4  col-sm-4 mix work-item <?php echo get_the_excerpt(); ?> wv_20" style="display: inline-block;">
					<div class="grid">
					 <div class="col-md-4 col-lg-4  col-sm-4 option2">
					<figure class="effect-winston">
						<img src="<?php echo $image[0]; ?>" >
						<figcaption>
							 <h2><?php echo get_the_title(); ?><span><?php echo get_the_content(); ?></span></h2>
											<p>
									         <!--<a href="#" class="button3 b3"><i class="fa fa-fw fa-star-o"></i></a>	
						                     <a href="#" class="button3 b3"><i class="fa fa-fw fa-comments-o"></i></a>-->
						                     <a href="<?php echo get_permalink(); ?>"  class="button3 b3"><i class="fa fa-external-link"></i></a>
                                           </p>
						</figcaption>			
					</figure>
					</div>
					</div>
					<figcaption class="overlay">		
					</figcaption>
				</figure>	
	<?php 
		$p++;
		endwhile;
		endif;
	?>
	
				
			</div>
          </div>			
	 </div>
</div>
	
	

	 <div class="wrapper ww">
		    <div class="container">
			    <div class="row">
				         <!--training start  -->
				           <div class="col-md-12 training">  
						    <navvv class="cl-effect-14 dark" id="cl-effect-14">
					                <a href="#cl-effect-14">TRAINING  <span class="change">  PROGRAM</span></a>
					
				            </navvv>
						        
								 <img class="position" src="<?php echo get_template_directory_uri(); ?>/image/shape3.png">
						  
						   
						     <div class="col-md-offset-1 col-md-12 ten">
	                           	<?php
	$my_query = new WP_Query('category_name=training&post_per_page=1&order=ASC'); 
    while ($my_query->have_posts()) :
    $my_query->the_post();
    $do_not_duplicate = $post->ID;
	$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
    ?>
							<?php echo get_the_content(); ?>						
								 <div class="col-md-7 image">
						            <img src="<?php echo $image[0]; ?>">
					            </div>
							<?php 
    //$p++;
    endwhile;
    ?> 
								
							</div>	
   						  
		                    </div>
					 
                         <!-- training end  -->					 
	            </div>
            </div>
  </div>
	
	
	
	 <div class="wrapper w4">
		    <div class="container">
			    <div class="row">
				         <!--slidder start  -->
				            <div class="col-md-12 client">  
							 <navvv class="cl-effect-14" id="cl-effect-14">
					            <a href="#cl-effect-14"><span class="change">  CLIENT</span> FEEDBACK</a>
					
				                </navvv>
								<img class="big" src="<?php echo get_template_directory_uri(); ?>/image/Shape2.png">
							
							
							
							<ul class="rslides ready rslides1">
							<?php 
   $args= array('post_type'=> 'testimonial', 'posts_per_page'=>3,'order'=>'ASC');
   query_posts($args);		
$p=1;
if ( have_posts() ) : while ( have_posts() ) : the_post(); 
$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 

	?>
	 <li id="rslides1_s<?php echo get_the_excerpt(); ?>" class="<?php if($p==1){ echo 'rslides1_on style_class';}else{ echo 'style_class1'; }?>" > 
									
									<img src="<?php echo $image[0]; ?>">
 
                                      <h2><?php echo get_the_title(); ?></h2>
  
                                      <p><?php echo get_the_content(); ?></p>
                                   
  
                                    </li>	
	 
<?php
		$p++;
		endwhile;
		endif;
	?>
                             </ul>											
			             </div>
                         <!-- slidder end  -->					 
	            </div>
            </div>
  </div>

	
<div class="clear"></div>
		<section class="section-white sec">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
							<div class="widget-title">
								<h4>Latest News</h4>
								<hr>
							</div><!-- end widget-title -->

							<ul class="latest-news">
							<?php
		             $args= array('post_type'=> 'latest_news', 'posts_per_page'=>2,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 $image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
					 ?>
                     <li>
									<img src="<?php echo $image[0]; ?>" alt="" class="img-responsive img-rounded alignleft">
									<h4><a href="#" title=""><?php echo get_the_title(); ?></a></h4>
									<p><?php echo get_the_content(); ?><a href="#">Read More</a></p>
									<span>Posted by <a href="#"><?php echo get_post_meta(get_the_ID(),'author',true);?></a> / Posted on <?php echo get_post_meta(get_the_ID(),'post_date',true);?></span>
								</li>       
									 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
								
							</ul><!-- end latest-course -->

						</div><!-- end widget -->
					</div><!-- end col -->

					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
							<div class="widget-title">
								<h4>Skills</h4>
								<hr>
							</div><!-- end widget-title -->
							
								<?php
		             $args= array('post_type'=> 'skill', 'posts_per_page'=>5,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>
					 <div class="skillbar clearfix " data-percent="<?php echo get_the_content(); ?>%">
	<div class="skillbar-title" style="background: #df4a43;"><span><?php echo get_the_title(); ?></span></div>
	<div class="skillbar-bar" style="background: #dc7570;"></div>
	<div class="skill-bar-percent"><?php echo get_the_content(); ?>%</div>
</div> <!-- End Skill Bar -->
		          <?php 
				     //$p++;
				     endwhile;
		             endif;?>
			
				  		</div><!-- end widget -->
			  		</div><!-- end col -->
			  	</div><!-- end row -->
		  	</div><!-- end container -->
	  	</section><!-- end section-white -->

		    <div class="wrapper w5">
				<div class="container">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="widget about-widget wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
							
							 <navvv class="cl-effect-14 dark" id="cl-effect-14">
					<a href="#cl-effect-14">OUR <span class="change">  CLIENTS</span></a>
				
				</navvv>

		                    <div id="clients" class="clearfix text-center">
							<?php 
   $args= array('post_type'=> 'clients', 'posts_per_page'=>12,'order'=>'ASC');
   query_posts($args);		
$p=1;
if ( have_posts() ) : while ( have_posts() ) : the_post(); 
$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 

	?>
	<div class="client-image">
		                    		<a href="#"><img src="<?php echo $image[0]; ?>" alt="" class="img-responsive"></a>
		                    	</div>
	<?php 
		$p++;
		endwhile;
		endif;
	?>
		                    	<!-- end client-image -->
		                    	
		                    </div><!-- end clients -->
						</div><!-- end widget -->
					</div><!-- end col -->
				</div><!-- end row -->
            </div>
			

<?php get_footer(); ?>
