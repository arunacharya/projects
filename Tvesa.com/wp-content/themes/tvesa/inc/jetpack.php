<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package tvesa
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function tvesa_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'tvesa_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function tvesa_jetpack_setup
add_action( 'after_setup_theme', 'tvesa_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function tvesa_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function tvesa_infinite_scroll_render
